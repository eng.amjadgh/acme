<?php
namespace App\Repositories;

use App\Models\Project;

class ProjectRepository
{
    /**
     * @var project
     */
    protected $project;

    /**
     * ProjectRepository constructor.
     *
     * @parm Project $projcet
     */
    public function __construct(Project $project)
    {
        $this->project = $project;
    }

    /**
     * getAllProjects function
     *
     * @return array
     */
    public function getAllProjects() {
        return Project::orderBy('id', 'desc')->take(10)->get();
    }
    /**
     * findById function
     *
     * @param $projectId
     * @return array
     */
    public function findById($projectId) {
        return Project::find($projectId);
    }

    /**
     * getAllProjectsAjax function
     *
     * @return array
     */
    public function getAllProjectsAjax() {
        return Project::select([
            'id',
            'title',
            'href',
            'image',
            'meta_tags'
        ])->get();
    }

    /**
     * getLastProjects function
     *
     * @param $lastN
     * @return array
     */
    public function getLastProjects($lastN) {
        return Project::orderBy('id', 'desc')->take($lastN)->get();
    }

    /**
     * createProject
     *
     * @param $project body
     *
     * @return $projectId
     */
    public function create($data) {
        return Project::create($data)->id;
    }

    /**
     * createProject
     *
     * @param $project body
     *
     * @return $projectId
     */
    public function update($projectId, $data) {
        $project = Project::find($projectId);
        return $project->update($data);
    }

    /**
     * deleteProject
     *
     * @param Project $project
     *
     * @return 1
     */
    public function delete(Project $project) {
        return $project->delete();
    }
}
