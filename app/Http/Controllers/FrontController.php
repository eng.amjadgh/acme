<?php

namespace App\Http\Controllers;

use App\Services\ProjectServices;
use Illuminate\Http\Request;

class FrontController extends Controller
{
    public function __construct(ProjectServices $projectServices) {
        $this->projectServices = $projectServices;
    }

    public function index() {
        $projects = $this->projectServices->getAllProjects();

        return view('home')->with('projects',$projects);
    }
}
