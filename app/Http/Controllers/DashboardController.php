<?php

namespace App\Http\Controllers;

use App\Services\ProjectServices;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function __construct(ProjectServices $projectServices) {
        $this->projectServices = $projectServices;
    }

    public function home() {
        $lastN = 5;
        $lastProjects = $this->projectServices->getLastProjects($lastN);

        return view('dashboard.home')->with('lastProjects',$lastProjects);
    }

    public function projects() {
        $projects = $this->projectServices->getAllProjects();

        return view('dashboard.projects')->with('projects',$projects);
    }
}
