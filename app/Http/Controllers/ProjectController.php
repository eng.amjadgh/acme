<?php

namespace App\Http\Controllers;

use App\Services\ProjectServices;
use Illuminate\Http\Request;
use Response;

class ProjectController extends Controller
{

    private ProjectServices $projectServices;

    public function __construct(ProjectServices $projectServices) {
        $this->projectServices = $projectServices;
    }

    public function index() {
        $projects = $this->projectServices->getAllProjects();
        var_dump($projects);
    }

    public function getAjax() {
        $projects = $this->projectServices->getAllProjectsAjax();
        $res['data'] = $projects;
        return Response::json( $res);
    }

    public function add(Request $request) {
        $this->validate(request(), [
            'title' => 'required|string',
            'meta_tags' => 'required|string',
            'image'=>'required|string'
        ]);
        $projectId = $this->projectServices->createProject($request->all());
        return Response::json( $projectId, 200);
    }

    public function get($projectId) {
        $project = $this->projectServices->getProject($projectId);
        return Response::json( $project, 200);
    }

    public function edit($projectId, Request $request) {
        $this->validate(request(), [
            'title' => 'required|string',
            'meta_tags' => 'required|string',
            'image'=>'required|string'
        ]);
        $projectId = $this->projectServices->updateProject($projectId, $request->all());
        return Response::json( $projectId, 200);
    }

    public function delete($projectId) {
        $project = $this->projectServices->deleteProject($projectId);
        return Response::json( $project, 200);
    }

}
