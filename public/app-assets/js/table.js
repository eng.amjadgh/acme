/**
 * DataTables Basic
 */
$( document ).ready(function() {

    var dt_projects_table = $('.datatable-projects'),
        assetPath = '/api/';

    if ($('body').attr('data-framework') === 'laravel') {
        assetPath = $('body').attr('data-asset-path');
    }

    // DataTable with buttons
    // --------------------------------------------------------------------

    if (dt_projects_table.length) {
        var dt_basic = dt_projects_table.DataTable({
            ajax: assetPath + 'projects',
            columns: [
                { data: 'id' },
                { data: 'id' }, // used for sorting so will hide this column
                { data: 'title' },
                { data: 'meta_tags' },
                { data: 'href' },
                { data: 'id' }
            ],
            columnDefs: [
                {
                    // For Responsive
                    className: 'control',
                    orderable: false,
                    responsivePriority: 2,
                    targets: 0
                },
                {
                    targets: 4,
                    render: function (data) {
                        if(data) {
                            return '<a target="_blank" href="'+data+'">Click to Link</a>'
                        } else {
                            return 'Not Available';
                        }
                    }
                },
                {
                    // Actions
                    targets: 5,
                    title: 'Actions',
                    orderable: false,
                    render: function (data, type, full, meta) {
                        return (
                            '<div class="d-inline-flex">' +
                            '<a class="pr-1 dropdown-toggle hide-arrow text-primary" data-toggle="dropdown">' +
                            feather.icons['more-vertical'].toSvg({ class: 'font-small-4' }) +
                            '</a>' +
                            '<div class="dropdown-menu dropdown-menu-right">' +
                            '<a href="#!" data-id="'+data+'" class="dropdown-item delete-record">' +
                            feather.icons['trash-2'].toSvg({ class: 'font-small-4 mr-50' }) +
                            'Delete</a>' +
                            '</div>' +
                            '</div>' +
                            '<a href="#!" data-projectID="'+data+'" data-toggle="modal" data-target="#modals-slide-up" class="item-edit">' +
                            feather.icons['edit'].toSvg({ class: 'font-small-4' }) +
                            '</a>'
                        );
                    }
                }
            ],
            order: [[2, 'desc']],
            dom:
                '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-right"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            displayLength: 7,
            lengthMenu: [7, 10, 25, 50, 75, 100],
            buttons: [
                {
                    text: feather.icons['plus'].toSvg({ class: 'mr-50 font-small-4' }) + 'Add New Record',
                    className: 'create-new btn btn-primary',
                    attr: {
                        'data-toggle': 'modal',
                        'data-target': '#modals-slide-in'
                    },
                    init: function (api, node, config) {
                        $(node).removeClass('btn-secondary');
                    }
                }
            ],
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.modal({
                        header: function (row) {
                            var data = row.data();
                            return 'Details of ' + data['full_name'];
                        }
                    }),
                    type: 'column',
                    renderer: function (api, rowIdx, columns) {
                        var data = $.map(columns, function (col, i) {
                            console.log(columns);
                            return col.title !== '' // ? Do not show row in modal popup if title is blank (for check box)
                                ? '<tr data-dt-row="' +
                                col.rowIndex +
                                '" data-dt-column="' +
                                col.columnIndex +
                                '">' +
                                '<td>' +
                                col.title +
                                ':' +
                                '</td> ' +
                                '<td>' +
                                col.data +
                                '</td>' +
                                '</tr>'
                                : '';
                        }).join('');

                        return data ? $('<table class="table"/>').append(data) : false;
                    }
                }
            },
            language: {
                paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            },
            fnDrawCallback: function ( oSettings ){
                $(".item-edit").click(function() {
                    let id = $(this).attr('data-projectid');
                    $.ajax({
                        url: "/dashboard/projects/show/"+id,
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        type: "get",
                        success: function(result) {
                            $('.update-record .dt-id').val(result.id);
                            $('.update-record .dt-name').val(result.title);
                            $('.update-record .dt-meta_tags').val(result.meta_tags);
                            $('.update-record .dt-link').val(result.href);
                            $('.update-record #thumbnailup').val(result.image);
                        }
                    });
                });
            }
        });
        $('div.head-label').html('<h6 class="mb-0">List All Projects</h6>');
    }

    // Add New record
    // ? Remove/Update this code as per your requirements ?
    $('.data-submit').on('click', function () {
        var $new_name = $('.add-new-record .dt-name').val(),
            $new_meta_tags = $('.add-new-record .dt-meta_tags').val(),
            $new_link = $('.add-new-record .dt-link').val(),
            $new_image = $('.add-new-record #thumbnail').val();
        if ($new_name != '') {
            $.ajax({
                url: "/dashboard/projects/create/",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                type: "post",
                data: {
                    title: $new_name,
                    href: $new_link,
                    image: $new_image,
                    meta_tags: $new_meta_tags
                },
                success: function(result) {
                    toastr['success'](
                        'successful operation!',
                        '👋 Add Success !',
                        {
                            closeButton: true,
                            tapToDismiss: false
                        }
                    );
                    dt_basic.row
                        .add({
                            responsive_id: result,
                            id: result,
                            title: $new_name,
                            meta_tags: $new_meta_tags,
                            link: $new_link
                        })
                        .draw();
                    $('#modals-slide-in').modal('hide');
                    $('.add-new-record .dt-name').val('');
                    $('.add-new-record .dt-meta_tags').val('');
                    $('.add-new-record .dt-link').val('');
                    $('.add-new-record #thumbnail').val('');
                }
            });
        }
    });


    $('.data-update').on('click', function () {
        var $id = $('.update-record .dt-id').val(),
            $new_name = $('.update-record .dt-name').val(),
            $new_meta_tags = $('.update-record .dt-meta_tags').val(),
            $new_link = $('.update-record .dt-link').val(),
            $new_image = $('.update-record #thumbnailup').val();
        if ($new_name != '') {
            $.ajax({
                url: "/dashboard/projects/edit/"+$id,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                type: "put",
                data: {
                    title: $new_name,
                    href: $new_link,
                    image: $new_image,
                    meta_tags: $new_meta_tags
                },
                success: function(result) {
                    toastr['success'](
                        'successful operation!',
                        '👋 Update Success !',
                        {
                            closeButton: true,
                            tapToDismiss: false
                        }
                    );
                    $('#modals-slide-up').modal('hide');
                    window.location.reload();
                }
            });
        }
    });

    // Delete Record
    $('.datatable-projects tbody').on('click', '.delete-record', function () {
        let row = $(this).parents('tr');
        $.ajax({
            url: "/dashboard/projects/delete/"+$(this).attr('data-id'),
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            type: "delete",
            success: function(result) {
                toastr['success'](
                    'successful operation!',
                    '👋 Delete Success !',
                    {
                        closeButton: true,
                        tapToDismiss: false
                    }
                );
                dt_basic.row(row).remove().draw();
            }
        });
    });

    $('#lfm').filemanager('image');
    $('#lfmup').filemanager('image');
});
