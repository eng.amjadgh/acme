/*=========================================================================================
    File Name: dashboard-ecommerce.js
    Description: dashboard ecommerce page content with Apexchart Examples
    ----------------------------------------------------------------------------------------
    Item Name: Vuexy  - Vuejs, HTML & Laravel Admin Dashboard Template
    Author: PIXINVENT
    Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

$(window).on('load', function () {
  'use strict';

  // On load Toast
  setTimeout(function () {
    toastr['success'](
      'You can start to edit!',
      '👋 Welcome !',
      {
        closeButton: true,
        tapToDismiss: false
      }
    );
  }, 2000);


});
