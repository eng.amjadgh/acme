<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes([
    'register' => false, // Register Routes...
    'reset' => false, // Reset Password Routes...
    'verify' => false, // Email Verification Routes...
]);

Route::group(['middleware' => ['auth']], function() {
    Route::group(['prefix' => 'dashboard'], function () {
        Route::get('/', [\App\Http\Controllers\DashboardController::class, 'home'])->name('dashboard.home');
        Route::get('/index', [\App\Http\Controllers\DashboardController::class, 'home']);
        Route::get('/projects', [\App\Http\Controllers\DashboardController::class, 'projects'])->name('dashboard.projects');

        Route::post('/projects/create', [\App\Http\Controllers\ProjectController::class, 'add'])->name('dashboard.projects.create');
        Route::get('/projects/show/{id}', [\App\Http\Controllers\ProjectController::class, 'get'])->name('dashboard.projects.get');
        Route::put('/projects/edit/{id}', [\App\Http\Controllers\ProjectController::class, 'edit'])->name('dashboard.projects.edit');
        Route::delete('/projects/delete/{id}', [\App\Http\Controllers\ProjectController::class, 'delete'])->name('dashboard.projects.delete');

    });

    Route::group(['prefix' => 'filemanager', 'middleware' => ['web', 'auth']], function () {
        \UniSharp\LaravelFilemanager\Lfm::routes();
    });
});

Route::get('/', [\App\Http\Controllers\FrontController::class, 'index'])->name('home');
Route::get('/home', [\App\Http\Controllers\FrontController::class, 'index']);
