<!doctype html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Dashboard to manage project of website">
    <meta name="keywords" content="admin dashboard, manage projects">
    <meta name="author" content="Amjad Ghniem">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="apple-touch-icon" href="{{asset('app-assets/images/ico/apple-icon-120.png')}}">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('app-assets/images/ico/favicon.ico')}}">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/vendors.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/extensions/toastr.min.css')}}">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/bootstrap-extended.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/colors.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/components.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/themes/dark-layout.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/themes/bordered-layout.css')}}">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/core/menu/menu-types/horizontal-menu.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/plugins/extensions/ext-component-toastr.css')}}">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}">
    @stack('styles')
    <!-- END: Custom CSS-->

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->
<body class="horizontal-layout horizontal-menu  navbar-floating footer-static  " data-open="hover" data-menu="horizontal-menu" data-col="">

    <!-- BEGIN: Header-->
    <nav class="header-navbar navbar-expand-lg navbar navbar-fixed align-items-center navbar-shadow navbar-brand-center" data-nav="brand-center">
        <div class="navbar-header d-xl-block d-none">
            <ul class="nav navbar-nav">
                <li class="nav-item"><a class="navbar-brand" href="/">
                        <span class="brand-logo">
                                <svg width="142" height="37" viewBox="0 0 142 37" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M21.8464 17.3913L15.1834 0.706543H14.6918L7.97412 17.3913H21.8464ZM14.9103 4.72828L19.2249 15.7609H10.5957L14.9103 4.72828Z" fill="#00B8D4"/>
                                    <path d="M27.3625 35.9241H29.2741L22.611 19.5654H7.26408L0.546387 35.9241H2.51254L8.411 21.1959H21.4095L27.3625 35.9241Z" fill="#00B8D4"/>
                                    <path d="M36.9203 18.3694C36.9203 18.0433 36.9203 17.7715 36.9203 17.3911H34.9541C34.9541 17.7715 34.9541 18.0433 34.9541 18.3694C34.9541 18.6955 34.9541 18.9672 34.9541 19.565H36.9203C36.9203 18.9672 36.9203 18.6955 36.9203 18.3694Z" fill="#00B0FF"/>
                                    <path d="M53.7418 1.73913C57.5103 1.73913 61.0057 2.98913 63.8457 5.16304L64.9926 3.8587C61.8795 1.3587 58.0018 0 53.7418 0C43.911 0 36.5926 6.73913 35.3364 15.6522H37.1387C38.3949 7.77174 44.9487 1.73913 53.7418 1.73913Z" fill="#00B0FF"/>
                                    <path d="M53.7417 35.0545C44.9487 35.0545 38.3948 29.511 37.1387 21.1958H35.2817C36.5379 30.5436 43.911 36.7393 53.6871 36.7393C57.9471 36.7393 61.8794 35.3806 64.9379 32.8806L63.791 31.5762C60.951 33.7501 57.4556 35.0545 53.7417 35.0545Z" fill="#00B0FF"/>
                                    <path d="M90.771 14.3478L73.731 0.706543V3.09785L90.771 16.6848L107.592 3.09785V0.706543L90.771 14.3478Z" fill="#536DFE"/>
                                    <path d="M90.771 19.0759L73.731 5.43457V35.3259H75.8063V9.34761L90.771 21.4672L105.954 9.34761V35.3259H107.483V5.43457L90.771 19.0759Z" fill="#536DFE"/>
                                    <path d="M120.7 17.9346H138.177V16.3042H120.7V3.8042H119.062V35.3259H141.454V33.6955H120.7V17.9346Z" fill="#7C4DFF"/>
                                    <path d="M141.454 16.3042H139.816V17.9346H141.454V16.3042Z" fill="#7C4DFF"/>
                                    <path d="M141.454 0.543457H119.062V2.17389H141.454V0.543457Z" fill="#7C4DFF"/>
                                </svg>
                        </span>
                        <h2 class="brand-text mb-0">Dashboard</h2>
                    </a></li>
            </ul>
        </div>
        <div class="navbar-container d-flex content">
            <div class="bookmark-wrapper d-flex align-items-center">
                <a class="nav-link nav-link-style"><i class="ficon" data-feather="moon"></i></a>
            </div>
            <ul class="nav navbar-nav align-items-center ml-auto">
                <li class="nav-item dropdown dropdown-user">
                    <a class="nav-link dropdown-toggle dropdown-user-link" id="dropdown-user" href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <div class="user-nav d-sm-flex d-none">
                            <span class="user-name font-weight-bolder">{{auth::user()->name}}</span>
                            <span class="user-status">{{auth::user()->email}}</span>
                        </div>
                        <span class="avatar">
                            <img class="round" src="{{asset('app-assets/images/portrait/small/avatar-s-11.jpg')}}" alt="avatar" height="40" width="40">
                            <span class="avatar-status-online"></span>
                        </span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-user">
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="mr-50" data-feather="power"></i> Logout</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
    <!-- END: Header-->


    <!-- BEGIN: Main Menu-->
    <div class="horizontal-menu-wrapper">
        <div class="header-navbar navbar-expand-sm navbar navbar-horizontal floating-nav navbar-light navbar-shadow menu-border" role="navigation" data-menu="menu-wrapper" data-menu-type="floating-nav">
            <div class="navbar-header">
            </div>
            <div class="shadow-bottom"></div>
            <!-- Horizontal menu content-->
            <div class="navbar-container main-menu-content" data-menu="menu-container">
                <h3>Welcome {{auth::user()->name}}, Edit your projects now</h3>
            </div>
        </div>
    </div>
    <!-- END: Main Menu-->

    <!-- BEGIN: Content-->

    @yield('content')

    <!-- END: Content-->

    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>

    <!-- BEGIN: Footer-->
    <footer class="footer footer-static footer-light">
        <p class="clearfix mb-0"><span class="float-md-left d-block d-md-inline-block mt-25">COPYRIGHT &copy; 2022<a class="ml-25" href="" target="_blank">Amjad Ghneim</a><span class="d-none d-sm-inline-block">, All rights Reserved</span></span><span class="float-md-right d-none d-md-block">Hand-crafted & Made with<i data-feather="heart"></i></span></p>
    </footer>
    <button class="btn btn-primary btn-icon scroll-top" type="button"><i data-feather="arrow-up"></i></button>
    <!-- END: Footer-->


    <!-- BEGIN: Vendor JS-->
    <script src="{{asset('app-assets/vendors/js/vendors.min.js')}}"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{asset('app-assets/vendors/js/ui/jquery.sticky.js')}}"></script>
    <script src="{{asset('app-assets/vendors/js/extensions/toastr.min.js')}}"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{asset('app-assets/js/core/app-menu.js')}}"></script>
    <script src="{{asset('app-assets/js/core/app.js')}}"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{asset('app-assets/js/dashboard.js')}}"></script>
    <!-- END: Page JS-->

    <!-- BEGIN: Custom JS -->
    @stack('scripts')
    <!-- END: Custom JS -->

    <script>
        $(window).on('load', function() {
            if (feather) {
                feather.replace({
                    width: 14,
                    height: 14
                });
            }
        })
    </script>
</body>
</html>
