@extends('dashboard.layout')

@section('content')

    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Dashboard Starts -->
                <section id="dashboard-ecommerce">
                    <div class="row match-height">
                        <!-- Projects Table Card -->
                        <div class="col-lg-8 col-12">
                            <div class="card card-company-table">
                                <div class="card-body p-0">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th>Title</th>
                                                <th>Meta Tags</th>
                                                <th>Linkable</th>
                                                <th>Created At</th>
                                                <th>Updated At</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($lastProjects as $project)
                                                <tr>
                                                    <td>
                                                        <div class="d-flex align-items-center">
                                                            <div class="avatar rounded">
                                                                <div class="avatar-content">
                                                                    <img src="{{$project->image}}" alt="Toolbar svg" />
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="font-weight-bolder">{{$project->title}}</div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        {{$project->meta_tags}}
                                                    </td>
                                                    <td>
                                                        @if($project->href)
                                                            <div class="d-flex align-items-center">
                                                                <div class="avatar bg-light-primary mr-1">
                                                                    <div class="avatar-content">
                                                                        <a href="{{$project->href}}" target="_blank">
                                                                            <i data-feather="monitor" class="font-medium-3"></i>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                <span><a href="{{$project->href}}">{{$project->href}}</a></span>
                                                            </div>
                                                        @else
                                                            <span>Not Available</span>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        {{$project->created_at}}
                                                    </td>
                                                    <td>
                                                        @if($project->updated_at)
                                                            {{$project->updated_at}}
                                                        @else
                                                            Not Updated yet
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <a href="{{route('dashboard.projects')}}" class="btn btn-primary">View More</a>
                            </div>
                        </div>
                        <!--/ Projects Table Card -->

                        <!-- Info Card -->
                        <div class="col-lg-4 col-md-6 col-12">
                            <div class="card card-developer-meetup">
                                <div class="meetup-img-wrapper rounded-top text-center">
                                    <img src="{{asset('app-assets/images/illustration/email.svg')}}" alt="Meeting Pic" height="170" />
                                </div>
                                <div class="card-body">
                                    <div class="meetup-header d-flex align-items-center">
                                        <div class="meetup-day">
                                            <h6 class="mb-0"><?= date("D") ?></h6>
                                            <h3 class="mb-0"><?= date("d") ?></h3>
                                        </div>
                                        <div class="my-auto">
                                            <h4 class="card-title mb-25">Damazzle Dashboard</h4>
                                            <p class="card-text mb-0">GO Ahead</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <div class="avatar bg-light-primary rounded mr-1">
                                            <div class="avatar-content">
                                                <i data-feather="calendar" class="avatar-icon font-medium-3"></i>
                                            </div>
                                        </div>
                                        <div class="media-body">
                                            <h6 class="mb-0"><?= date("D, M d, Y") ?></h6>
                                            <small><?= date("h:i a") ?></small>
                                        </div>
                                    </div>
                                    <div class="media mt-2">
                                        <div class="avatar bg-light-primary rounded mr-1">
                                            <div class="avatar-content">
                                                <i data-feather="map-pin" class="avatar-icon font-medium-3"></i>
                                            </div>
                                        </div>
                                        <div class="media-body">
                                            <h6 class="mb-0">Damascus</h6>
                                            <small>
                                                <?php
                                                echo $ip = isset($_SERVER['HTTP_CLIENT_IP'])
                                                    ? $_SERVER['HTTP_CLIENT_IP']
                                                    : (isset($_SERVER['HTTP_X_FORWARDED_FOR'])
                                                        ? $_SERVER['HTTP_X_FORWARDED_FOR']
                                                        : $_SERVER['REMOTE_ADDR']);
                                                ?>
                                            </small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/ Info Card -->
                    </div>
                </section>
                <!-- Dashboard ends -->

            </div>
        </div>
    </div>
@endsection
